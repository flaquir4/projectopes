# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table categoria (
  nom                       varchar(255) not null,
  id_categoria              integer,
  descripcio                varchar(255),
  motiu_de_la_proposta      varchar(255),
  constraint pk_categoria primary key (nom))
;

create table categoria_proposada (
  nom                       varchar(255) not null,
  id_categoria              integer,
  descripcio                varchar(255),
  motiu_de_la_proposta      varchar(255),
  constraint pk_categoria_proposada primary key (nom))
;

create table oferta (
  id_servei                 integer not null,
  data_cad                  timestamp,
  descripcio                varchar(255),
  es_puntual                boolean,
  hores_estimades           integer,
  nom                       varchar(255),
  esta_realitzat            boolean,
  usuari_dni                varchar(255),
  nom_categoria             varchar(255),
  constraint pk_oferta primary key (id_servei))
;

create table servei (
  id_servei                 integer not null,
  data_cad                  timestamp,
  descripcio                varchar(255),
  es_puntual                boolean,
  hores_estimades           integer,
  nom                       varchar(255),
  esta_realitzat            boolean,
  usuari_dni                varchar(255),
  nom_categoria             varchar(255),
  constraint pk_servei primary key (id_servei))
;

create table usuari (
  dni                       varchar(255) not null,
  data_naixement            timestamp,
  nom                       varchar(255),
  sexe                      varchar(255),
  email                     varchar(255),
  es_conflictiu             boolean,
  saldo_hores               integer,
  constraint pk_usuari primary key (dni))
;

create sequence categoria_seq;

create sequence categoria_proposada_seq;

create sequence oferta_seq;

create sequence servei_seq;

create sequence usuari_seq;

alter table oferta add constraint fk_oferta_usuari_1 foreign key (usuari_dni) references usuari (dni) on delete restrict on update restrict;
create index ix_oferta_usuari_1 on oferta (usuari_dni);
alter table servei add constraint fk_servei_usuari_2 foreign key (usuari_dni) references usuari (dni) on delete restrict on update restrict;
create index ix_servei_usuari_2 on servei (usuari_dni);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists categoria;

drop table if exists categoria_proposada;

drop table if exists oferta;

drop table if exists servei;

drop table if exists usuari;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists categoria_seq;

drop sequence if exists categoria_proposada_seq;

drop sequence if exists oferta_seq;

drop sequence if exists servei_seq;

drop sequence if exists usuari_seq;

