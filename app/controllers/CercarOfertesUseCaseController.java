package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Oferta;

public class CercarOfertesUseCaseController {

	public List<List<String>> consultarOfertes() {
		DataControllerFactory dcfControllerFactory = DataControllerFactory.getInstance();
		ICtrlOfertes ctrlOferta = (ICtrlOfertes) dcfControllerFactory.getCtrlOFerta();
		ICtrlUsuari ctrlUsuari = dcfControllerFactory.getCtrlUsuari();
		List<Oferta> ofertes = ctrlOferta.all();
		List<List<String>> result = new ArrayList<List<String>>();
		for(Oferta oferta : ofertes){
			ArrayList <String> dades = oferta.getDades();
			String dni = dades.get(0);
			dades.set(0,ctrlUsuari.get(dni).getNom());
			result.add(dades);
		}
		return result;
	}

}
