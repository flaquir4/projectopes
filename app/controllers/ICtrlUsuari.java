package controllers;

import models.Usuari;

public interface ICtrlUsuari {
	public Usuari get(String dni);
}
