package controllers;

import java.util.ArrayList;
import java.util.List;
import models.Categoria;
import models.Oferta;
import models.Usuari;
import play.mvc.Result;

public class PublicarOfertaUseCaseController {
	String dni;
	public PublicarOfertaUseCaseController(String dni){
		this.dni = dni;
	}
	
	public List<String> mostrarCategories(){
		DataControllerFactory dcf = DataControllerFactory.getInstance();
		ICtrlCategoria CtrlCategoria = dcf.getCtrlCategoria();
		List<Categoria> categorias = CtrlCategoria.all();
		List<String> nombres = new ArrayList<String>();
		for(Categoria c : categorias ){
			nombres.add(c.getNom());
		}
		return nombres;
	}
	
	public void publicarOferta(String nom, String descripcio, boolean esPuntual, java.util.Date dataCad, int horesEstimades, String nomCategoria){
		DataControllerFactory dcf = DataControllerFactory.getInstance();
		ICtrlUsuari ctrlUsuari= dcf.getCtrlUsuari();
		ICtrlCategoria ctrlCategoria = dcf.getCtrlCategoria();
		Usuari user = ctrlUsuari.get(dni);
		Categoria cat = ctrlCategoria.get(nomCategoria);
		
		Oferta oferta = new Oferta(nom, descripcio, esPuntual, dataCad, horesEstimades, cat, user);
		oferta.save();
	}
}
