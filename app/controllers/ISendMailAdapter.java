package controllers;

import java.rmi.RemoteException;
import java.util.Date;


public interface ISendMailAdapter {
	public Boolean sendMail(String email, String nom, Date data) throws Exception;
}
