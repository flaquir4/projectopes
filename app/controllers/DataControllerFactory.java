package controllers;

import models.CtrlCategoria;
import models.CtrlOfertes;
import models.CtrlUsuari;

public class DataControllerFactory {
	
	private static DataControllerFactory instance;
	
	private DataControllerFactory(){
		
	}
	
	public static DataControllerFactory getInstance(){
		if(instance == null){
			instance = new DataControllerFactory();
		}
		return instance;
	}
	public ICtrlOfertes getCtrlOFerta(){
		return new CtrlOfertes();
		
	}
	
	public ICtrlUsuari getCtrlUsuari(){
		return (ICtrlUsuari) new CtrlUsuari();
	}
	
	public ICtrlCategoria getCtrlCategoria(){
		return (ICtrlCategoria) new CtrlCategoria();
	}
}
