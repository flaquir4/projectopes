package controllers;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import play.*;
import play.mvc.*;
import play.mvc.Http.Session;
import play.data.*;
import static play.data.Form.*;

import views.html.publica.*;
import views.html.login.*;
import models.*;

public class Application extends Controller {
	static Form<Oferta> ofertesForm = form(Oferta.class);
	static PublicarOfertaUseCaseController publicarOfertes;
	static List<String> cat;
	
	public static Result index() {

		CercarOfertesUseCaseController casdUs = new CercarOfertesUseCaseController();
		List<List<String>>result = casdUs.consultarOfertes();
		// PublicarOfertaUseCaseController publicarOfertes = new PublicarOfertaUseCaseController("3243434");

//		  String[] postAction = request().body().asText().get("action");
		Session session = Http.Context.current().session();
		String email = session.get("email");
		if(email == null) {
			return login();
		}
		return ok(views.html.index.render(result));
	}
	
	public static Result renderPublica(){
		 PublicarOfertaUseCaseController publicarOfertes;
		 publicarOfertes = new PublicarOfertaUseCaseController("42846283");
		 cat = publicarOfertes.mostrarCategories();
		 Session session = Http.Context.current().session();
		 String email = session.get("email");
			if(email == null) {
				return login();
			}
		return ok(views.html.publica.render(ofertesForm, cat));
		
	}
	
	public static Result publica(){
		 PublicarOfertaUseCaseController publicarOfertes;
		 publicarOfertes = new PublicarOfertaUseCaseController("42846283");

		  Form<Oferta> filledForm = ofertesForm.bindFromRequest();
		  if(filledForm.hasErrors()) {
			
	            return badRequest(views.html.publica.render(filledForm, cat));
	        } else {
		  Oferta ty = filledForm.get();
		  String nom = ty.getNom();
		  String descripcio = ty.getDescripcio();
		  Boolean esPuntual = ty.getEsPuntual();
		  Date dataCad = ty.getDataCad();
		  String nomCategoria = ty.getNomCategoria();
		  publicarOfertes.publicarOferta(nom, descripcio, esPuntual,  dataCad, 5,nomCategoria);
	        }
		  return redirect("/");
		
	}
	
	public static Result login() {
		Form<Login> loginForm = form(Login.class);
		return ok(
	    		
	        views.html.login.render(loginForm)
	    );
	}
	

		public static Result authenticate() {
		    Form<Login> loginForm = form(Login.class).bindFromRequest();
		    if (loginForm.hasErrors()) {
		        return badRequest(views.html.login.render(loginForm));
		    } else {
		        session().clear();
		        session("email", loginForm.get().email);
		        return redirect(
		            routes.Application.index()
		        );
		    }
		}
	
	public static class Login {

	    public String email;
	    public String password;
	 
	    public String validate() {
	        if (Usuari.authenticate(email, password) == null) {
	          return "Invalid user or password";
	        }
	        return null;
	    }
	}
}
