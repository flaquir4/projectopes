package controllers;

import java.util.List;

import models.Categoria;

public interface ICtrlCategoria {
public List<Categoria> all();
public Categoria get(String nomCategoria);
}
