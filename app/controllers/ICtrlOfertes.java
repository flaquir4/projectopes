package controllers;

import java.util.List;

import models.Oferta;

public interface ICtrlOfertes {
	List<Oferta> all();

}