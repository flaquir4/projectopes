import java.sql.Date;
import java.util.List;
import java.util.Map;

import models.Oferta;
import models.Person;
import models.Usuari;
import play.Application;
import play.GlobalSettings;
import play.libs.Yaml;

import com.avaje.ebean.Ebean;

public class Global extends GlobalSettings {
    
    public void onStart(Application app) {
        InitialData.insert(app);
    }
    
    static class InitialData {
        
        public static void insert(Application app) {
            if(Ebean.find(Oferta.class).findRowCount() == 0) {
                

				Map<String,List<Object>> all = (Map<String,List<Object>>)Yaml.load("initialData.yml");

                // Insert users first
              //s  Ebean.save(all.get("ofertes"));

                // Insert oferts
			//	Usuari user =   (Usuari) all.get("person");
			//	Usuari user = new Usuari((Date)pers.get(0), (String)pers.get(1), (String)pers.get(2), (String)pers.get(3), (String)pers.get(4),(String)pers.get(5));
                Ebean.save(all.get("person"));
                Ebean.save(all.get("ofertes"));
                Ebean.save(all.get("categories"));
               
                
            }
        }
        
    }
    
}