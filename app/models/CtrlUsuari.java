package models;

import controllers.ICtrlUsuari;

public class CtrlUsuari implements ICtrlUsuari {

	@Override
	public Usuari get(String dni) {
		return Usuari.find.byId(dni);
	}

}
