package models;

import java.util.Date;
import play.db.ebean.*;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

import play.data.validation.Constraints.Required;

@Entity
public class Usuari extends Person{
	
	


	
	public  String email;
	
	public static String password;
	
	Boolean esConflictiu;
	
	Integer saldoHores;
	
	public Usuari (Date date, String dni, String nom, String sexe, String email, String password){
		setDataNaixement(date);
		setDni(dni);
		setNom(nom);
		setSexe(sexe);
		this.email = email;
		this.password = password;
		this.esConflictiu = false;
		this.saldoHores =2;
	}
	


	public void inicializarOfertaUsuari(){
	
	}
	
	
	public String getDni(){
		return this.dni;
	}
	public String getNom(){
		return this.nom;
	}
	public static Finder<String,Usuari> find = new Finder<String,Usuari>(
	        String.class, Usuari.class
	    );

	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public static String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public Boolean getEsConflictiu() {
		return esConflictiu;
	}



	public void setEsConflictiu(Boolean esConflictiu) {
		this.esConflictiu = esConflictiu;
	}



	public Integer getSaldoHores() {
		return saldoHores;
	}



	public void setSaldoHores(Integer saldoHores) {
		this.saldoHores = saldoHores;
	} 
	public static String authenticate(String user, String password){
		if(password.equals("admin")&& user.contains("@")) return "ola k ase";
		return null;
	}
}
