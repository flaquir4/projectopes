package models;

import java.util.Date;

import play.mvc.Http;
import play.mvc.Http.Session;

import org.tempuri.Service1Locator;
import org.tempuri.Service1Soap;


import controllers.ISendMailAdapter;

public class SendMailAdapter implements ISendMailAdapter{

	@Override
	public Boolean sendMail(String email, String nom, Date data) throws Exception {
		Service1Locator locator = new Service1Locator();
		Service1Soap s = locator.getService1Soap();
		String date = data.toString();
		Session session = Http.Context.current().session();
		String emai = session.get("email");
		Boolean b = s.sendMail(email, nom, date, 1, "PESBankingPass");
		return true;
	}
	
}
