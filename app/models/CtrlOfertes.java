package models;

import java.util.List;

import controllers.ICtrlOfertes;

public class CtrlOfertes implements ICtrlOfertes{

	@Override
	public List<Oferta> all() {
	
		return Oferta.find.all();
	}

}
