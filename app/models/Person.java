package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@MappedSuperclass
public abstract class Person extends Model {
	
	public Date dataNaixement;
	@Id
	public String dni;
	
	public String nom;
	
	public Date getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(Date dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String sexe;
	
	public String getNom(){
		return nom;
	}
}