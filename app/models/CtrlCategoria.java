package models;

import java.util.List;
import java.util.Set;


import controllers.ICtrlCategoria;

public class CtrlCategoria implements ICtrlCategoria{

	@Override
	public List<Categoria> all() {
		return Categoria.find.all();
	}
	@Override
	public Categoria get(String nomCategoria){
		return Categoria.find.byId(nomCategoria);
	}
}
