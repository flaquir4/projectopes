package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.UniqueConstraint;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
@Entity
public class CategoriaProposada extends Model {
	

	public CategoriaProposada(int id, String descripcio, String motiu,
			String nom) {
		this.idCategoria  = id;
		this.descripcio = descripcio;
		this.motiuDeLaProposta = motiu;
	}
	
	public int idCategoria;
	@Required
	public String descripcio;

	String motiuDeLaProposta;
	@Id
	public String nom;
	
	public String getNom(){
		return this.nom;
	}
}
