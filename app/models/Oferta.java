package models;

import javax.persistence.Entity;

import play.mvc.Http;
import play.mvc.Http.Session;

import controllers.ISendMailAdapter;

@Entity
public class Oferta extends Servei {
	
	
	/*public Oferta(String nom, String descripcio, Boolean esPuntual, Date dat,
			int horesEstimades, Categoria c) {
		super(nom, descripcio, esPuntual, dat, horesEstimades, c);
	}*/
	
	public Oferta(String nom, String descripcio, Boolean esPuntual, java.util.Date dataCad,
			int horesEstimades, Categoria c, Usuari usuari) {
		super(nom, descripcio, esPuntual, dataCad, horesEstimades, c, usuari);
		ISendMailAdapter sm = new SendMailAdapter();
	
			try {
				Session session = Http.Context.current().session();
				String email = session.get("email");
				sm.sendMail(email, nom, dataCad);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	}
	
	public static Finder<Long,Oferta> find = new Finder<Long,Oferta>(
		    Long.class, Oferta.class  ); 
	
	
}
