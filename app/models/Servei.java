package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Servei extends Model{

	@Id
	@GeneratedValue
	public int idServei;
	@Required

	public Date dataCad;
	public int getIdServei() {
		return idServei;
	}

	public void setIdServei(int idServei) {
		this.idServei = idServei;
	}

	public Date getDataCad() {
		return dataCad;
	}

	public void setDataCad(Date dataCad) {
		this.dataCad = dataCad;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public Boolean getEsPuntual() {
		return esPuntual;
	}

	public void setEsPuntual(Boolean esPuntual) {
		this.esPuntual = esPuntual;
	}

	public int getHoresEstimades() {
		return horesEstimades;
	}

	public void setHoresEstimades(int horesEstimades) {
		this.horesEstimades = horesEstimades;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Boolean getEstaRealitzat() {
		return estaRealitzat;
	}

	public void setEstaRealitzat(Boolean estaRealitzat) {
		this.estaRealitzat = estaRealitzat;
	}

	public Usuari getUsuari() {
		return usuari;
	}

	public void setUsuari(Usuari usuari) {
		this.usuari = usuari;
	}

	public String getNomCategoria() {
		return nomCategoria;
	}

	public void setNomCategoria(String nomCategoria) {
		this.nomCategoria = nomCategoria;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Required

	public String descripcio;
	@Required

	public Boolean esPuntual;
	@Required
	public int horesEstimades;
	@Required
	public	String nom;
	public Boolean estaRealitzat;
	
	@ManyToOne
	public Usuari usuari;
	@Required
	public String nomCategoria;

    Categoria categoria; 
	
	public Servei( String nom, String descripcio, Boolean esPuntual, Date dataCad2, int horesEstimades, Categoria c, Usuari user){
		this.estaRealitzat = false;
		this.nom = nom;
		this.descripcio = descripcio;
		this.esPuntual = esPuntual;
		this.dataCad = dataCad2;
		this.horesEstimades = horesEstimades;
		this.usuari = user;
		
		
		
	}
	
public ArrayList<String> getDades(){
		ArrayList<String> dades = new ArrayList<String>();
		dades.add(usuari.getDni());
		dades.add(this.descripcio);
		dades.add( String.valueOf(this.dataCad.getDate())+"/"+String.valueOf(this.dataCad.getMonth()+1)+"/"+String.valueOf(this.dataCad.getYear()+1900));
		return dades;
	}
	
}
