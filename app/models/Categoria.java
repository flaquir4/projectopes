package models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import play.db.ebean.Model.Finder;

@Entity
public class Categoria extends CategoriaProposada{
   
    public Categoria(int id, String descripcio, String motiu, String nom) {
		super(id, descripcio, motiu, nom);
	}
    
    
	//public Servei servei;
	public void inicialitzarServeiCategoria( Servei servei){
	//	this.servei = servei;
	}
	
	public static Finder<String,Categoria> find = new Finder<String,Categoria>(
		    String.class, Categoria.class  ); 
}
