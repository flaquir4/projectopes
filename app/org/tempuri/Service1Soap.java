/**
 * Service1Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface Service1Soap extends java.rmi.Remote {
    public boolean sendMail(java.lang.String to, java.lang.String name, java.lang.String date, int messageType, java.lang.String password) throws java.rmi.RemoteException;
}
