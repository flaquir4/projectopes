/**
 * Service1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */



package org.tempuri;
import javax.xml.rpc.*;
public interface Service1 extends Service {
    public java.lang.String getService1SoapAddress();

    public org.tempuri.Service1Soap getService1Soap() throws ServiceException;

    public org.tempuri.Service1Soap getService1Soap(java.net.URL portAddress) throws ServiceException;
}
