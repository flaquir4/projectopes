import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

import java.sql.Date;

import models.Categoria;
import models.CategoriaProposada;
import models.Oferta;
import models.Servei;
import models.Usuari;

import org.junit.Test;

import com.avaje.ebean.Ebean;

import play.mvc.Content;


/**
 *
 * Simple (JUnit) tests that can call all parts of a play app.
 * If you are interested in mocking a whole application, see the wiki for more details.
 *
 */
public class ApplicationTest {

	@Test 
	public void simpleCheck() {
		int a = 1 + 1;
		assertThat(a).isEqualTo(2);
	}

	@Test
	public void renderTemplate() {
		//Content html = views.html.index.render("Your new application is ready.");
	//	assertThat(contentType(html)).isEqualTo("text/html");
		//assertThat(contentAsString(html)).contains("Your new application is ready.");
	}


	@Test
	public void save() {
		running(fakeApplication(), new Runnable() {
			public void run() {
				// Here is your real test code
				Date dat = new Date(1990, 02, 02);

				Usuari sergio =   new Usuari(dat, "49375655S","Sergi" , "Dona", "student@gmail.com", "tontaco");
				sergio.save();
				Categoria cat = new Categoria(10, "hola k ase", "feo","xavi" );
				Oferta oferta = new Oferta("pegar xavi","fotre-li una clatejada",false, dat, 3, cat, sergio);
				oferta.save();
				Oferta ofer  = Ebean.find(Oferta.class,1);
				assertThat(ofer.usuari).isNotNull();
			}
		});
	}

	@Test
	public void categoriaProposada() {
		running(fakeApplication(), new Runnable() {
			public void run() {
				// Here is your real test code
				Date dat = new Date(1990, 02, 02);

				CategoriaProposada cat = new CategoriaProposada(10, "hola k ase", "feo","xavi" );
				assertThat(cat.idCategoria).isNotNull();
			}
		});
	}
	@Test
	public void categoria() {
		running(fakeApplication(), new Runnable() {
			public void run() {
				// Here is your real test code
				Date dat = new Date(1990, 02, 02);

				Categoria cat = new Categoria(10, "hola k ase", "feo","xavi" );
				assertThat(cat.idCategoria).isNotNull();
			}
		});
	}
	@Test
	public void servei() {
		running(fakeApplication(), new Runnable() {
			public void run() {
				// Here is your real test code
				Date dat = new Date(1990, 02, 02);
				Categoria cat = new Categoria(10, "hola k ase", "feo","xavi" );

		//		Servei serv = new Servei("pegar xavi","fotre-li una clatejada",false, dat, 3, cat);
			//	serv.save();
				Servei hi =  Ebean.find(Servei.class, 1);

				assertThat(hi.nom).isNotNull();
			}
		});



	}

}
